#tag Class
Protected Class App
Inherits ConsoleApplication
	#tag Event
		Function Run(args() as String) As Integer
		  Var tab() As Integer = generujTablice(10000, 10000)
		  wyswietlTablice(tab)
		  quicksort(tab)
		  wyswietlTablice(tab)
		  
		  Var czekaj As String = Input
		  
		  
		End Function
	#tag EndEvent


	#tag Method, Flags = &h21
		Private Function generujTablice(rozmiar as Integer, zakres as Integer) As Integer()
		  Var tab() As Integer
		  
		  For i As Integer = 0 To rozmiar - 1
		    tab.Add(System.Random.InRange(0, zakres-1))
		  Next
		  
		  return tab
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function podzielSortujTablice(tab() as Integer, indeksOd as Integer, indeksDo as Integer) As Integer
		  Var pivot As Integer = _
		  System.Random.InRange(indeksOd, indeksDo-1)
		  zamien(tab, indeksOd, pivot)
		  
		  Var wartosc As Integer = tab(indeksOd)
		  Var ix_wieksze As Integer = indeksOd + 1
		  Var ix_mniejsze As Integer = indeksDo - 1
		  
		  Do
		    
		    While (ix_wieksze <= ix_mniejsze _ 
		      And wartosc >= tab(ix_wieksze))
		      ix_wieksze = ix_wieksze +1
		    Wend
		    
		    While (tab(ix_mniejsze) > wartosc)
		      ix_mniejsze = ix_mniejsze - 1
		    Wend
		    
		    If ix_wieksze < ix_mniejsze Then
		      zamien(tab, ix_wieksze, ix_mniejsze)
		    End If
		    
		  Loop Until ( ix_wieksze >= ix_mniejsze)
		  
		  zamien(tab, indeksOd, ix_mniejsze)
		  
		  Return ix_mniejsze
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub quicksort(tab() as Integer)
		  quicksortWewnetrzny(tab, 0, tab.Count)
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub quicksortWewnetrzny(tab() as Integer, indexOd as Integer, indexDo as Integer)
		  Var podzial As Integer 
		  
		  If indexDo - indexOd > 1 Then
		    podzial = podzielSortujTablice(tab, indexOd, indexDo)
		    quicksortWewnetrzny(tab, indexOd, podzial)
		    quicksortWewnetrzny(tab, podzial+1, indexDo)
		  End 
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub wyswietlTablice(tab() as Integer)
		  Var wiersz As String
		  
		  For Each elem As Integer In tab
		    wiersz = wiersz + elem.ToString + ", "
		  Next
		  
		  wiersz = wiersz + EndOfLine
		  Print(wiersz)
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub zamien(tab() as Integer, indeksLewy as Integer, indeksPrawy as Integer)
		  Var temp As Integer = tab(indeksLewy)
		  tab(indeksLewy) = tab(indeksPrawy)
		  tab(indeksPrawy) = temp
		End Sub
	#tag EndMethod


	#tag ViewBehavior
	#tag EndViewBehavior
End Class
#tag EndClass
